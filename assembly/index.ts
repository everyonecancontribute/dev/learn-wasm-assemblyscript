// The entry file of your WebAssembly module.

export function add(a: i32, b: i32): i32 {
  return a + b;
}

export function fib(n: i32): i32 {
  var a = 0, b = 1
  if (n > 0) {
    while (--n) {
      let t = a + b
      a = b
      b = t
    }
    return b
  }
  return a
}

export function hello_from(n: string): string {
  let out = `Hello from ${n}`

  return out 
}

export function sort(n: i32[]): i32[] {
  let res: i32[]

  return n.sort((n1,n2) => n1 - n2)
}

export function format_arr(n: i32[]): string {
  return n.join(",")
}