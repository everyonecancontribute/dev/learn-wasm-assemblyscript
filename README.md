# Learn Wasm: AssemblyScript

Learning project for [getting started with AssemblyScript](https://www.assemblyscript.org/getting-started.html#setting-up-a-new-project) and WebAssembly, as a resource to continue where the meetup ended, or follow the learning path from the meetup recording. 

## Demo from the EveryoneCanContribute Meetup 

Fork the project, open it in Gitpod. It will build and run all components automatically. You can also open a read-only copy [in Gitpod](https://gitpod.io#https://gitlab.com/everyonecancontribute/dev/learn-wasm).

We have added the following functionality into [assembly/index.ts](assembly/index.ts):

- `add()` from asinit, but modified to print into a dedicated div id HTML element.
- `fib()` from the introduction
- `hello_from()` to receive a string, and return a `Hello from X` concatted string, printed in HTML
- `sort()` function interface, receiving an array of integers to sort, using a lambda expression as comparator. `format_arr()` uses join internally to format the output as string for printing inside the HTML body.

[index.html](index.html) imports the functions, and updates the div ids using Javascript. The code is minimalistic to allow easy copy-paste to see something immediately.

## Resources

- AssemblyScript getting started: https://www.assemblyscript.org/getting-started.html#setting-up-a-new-project
  - WASM Cooking: https://k33g.gumroad.com/l/wasmcooking 
- [53. EveryoneCanContribute Cafe: WebAssembly with AssemblyScript, first try in Gitpod](https://everyonecancontribute.com/post/2022-07-12-cafe-53-webassemply-assemblyscript-first-steps-gitpod/)

[![53. EveryoneCanContribute Cafe: WebAssembly with AssemblyScript, first try in Gitpod meetup recording](http://img.youtube.com/vi/H1O2j4w78j8/0.jpg)](https://www.youtube.com/watch?v=H1O2j4w78j8)

## Setup 

Done once.

### Pre-requistites

Open the empty project in Gitpod, and install the webassembly package into the pod environment. The Gitpod workspace image already comes pre-installed with NodeJS. 

```
$ nom install webassembly
```

### Initialize

```
$ npx asinit .
```

## Build/Run

### Build

```
$ npm run asbuild
```

### Run 

```
$ npm run start
```

## Errors

```
Uncaught TypeError: exports.__new is not a function
```

https://github.com/AssemblyScript/assemblyscript/issues/2244 

```
  "scripts": {
    "asbuild:debug": "asc assembly/index.ts --target debug --exportRuntime",
    "asbuild:release": "asc assembly/index.ts --target release --exportRuntime",
    "asbuild": "npm run asbuild:debug && npm run asbuild:release",
```    
